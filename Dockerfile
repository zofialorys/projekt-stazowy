FROM ruby:2.6.3-stretch
RUN apt-get update -qq

WORKDIR /project
COPY Gemfile ./Gemfile
COPY Gemfile.lock ./Gemfile.lock

RUN bundle install
RUN apt install curl
RUN curl -sL https://deb.nodesource.com/setup_10.x | bash - && apt-get install -y nodejs
COPY . /project
EXPOSE 3000


# Start the main process.
CMD ["rails", "server", "-b", "0.0.0.0"]
