# frozen_string_literal: true

Raven.configure do |config|
  config.current_environment = 'production'
end
