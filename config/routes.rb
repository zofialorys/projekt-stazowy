# frozen_string_literal: true

Rails.application.routes.draw do
  mount GraphiQL::Rails::Engine, at: '/graphiql', graphql_path: '/graphql' if Rails.env.development?
  post '/graphql', to: 'graphql#execute'
  scope '(:locale)', locale: /#{I18n.available_locales.join("|")}/ do
    devise_for :users, controllers: { registrations: 'user/registrations', sessions: 'user/sessions' }
    root to: 'main#index'
    resources 'organisation'
    resources 'team'
    namespace 'user' do
      resources 'conversations'
    end
  end
  namespace 'api' do
    namespace 'v1' do
      resources 'user'
    end
  end
end
