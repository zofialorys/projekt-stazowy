# frozen_string_literal: true

class ConversationInvitationMailer < ApplicationMailer
  default from: 'notifications@example.com'

  def invitation_email
    @user = params[:user]
    @conversation = params[:conversation]
    @url = user_conversation_url(id: @conversation.id)
    mail(to: @user.email, subject: t('you_are_invited_to_new_conversation'))
  end
end
