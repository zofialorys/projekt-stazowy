# frozen_string_literal: true

module Mutations
  class DestroyUser < ::Mutations::BaseMutation
    argument :id, Integer, required: true

    field :user, Types::UserType, null: true
    field :errors, [String], null: true

    def resolve(id:)
      user = User.find(id)
      if user.destroy
        {
          user: user,
          errors: []
        } else {
          user: nil,
          errors: user.errors.full_messages
        }
      end
    end
  end
end
