# frozen_string_literal: true

module Mutations
  class UpdateUser < ::Mutations::BaseMutation
    argument :id, Integer, required: true
    argument :email, String, required: false

    field :user, Types::UserType, null: true
    field :errors, [String], null: true

    def resolve(id:, **attributes)
      user = User.find(id)
      if user.update!(email: attributes[:email])
        {
          user: user,
          errors: []
        } else {
          user: nil,
          errors: user.errors.full_messages
        }
      end
    end
  end
end
