# frozen_string_literal: true

class ProjectSchema < GraphQL::Schema
  mutation(Types::MutationType)
  query(Types::QueryType)
end

GraphQL::Errors.configure(ProjectSchema) do
  rescue_from ActiveRecord::RecordNotFound do |e|
    GraphQL::ExecutionError.new "Record not found: id: #{e.id}, model: #{e.model}, primary_key: #{e.primary_key}."
  end
  rescue_from ActiveRecord::RecordInvalid do |e|
    error_messages = e.record.errors.full_messages.join("\n")
    GraphQL::ExecutionError.new "Validation failed: #{error_messages}."
  end

  rescue_from StandardError do |e|
    GraphQL::ExecutionError.new e.message
  end
end
