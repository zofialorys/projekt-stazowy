//= require jquery
//= require jquery_ujs
//= require popper
//= require bootstrap
//= require material
//= require moment
// If you require timezone data (see moment-timezone-rails for additional file options)
//= require moment-timezone-with-data
//= require tempusdominus-bootstrap-4


$(document).ready(function () {
    $("#conversation_image").change((e) => {
        const fileName = e.target.value.replace(/^.*[\\\/]/, "");
        $(".file-input .custom-file-label").text(fileName)
    })

    $('#conversation_date').datetimepicker({
        format: 'DD-MM-YYYY HH:mm',

        icons: {
            time: "fa fa-clock",
            date: "fa fa-calendar",
            up: "fa fa-arrow-up",
            down: "fa fa-arrow-down"
        }
    });
})