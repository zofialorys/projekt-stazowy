# frozen_string_literal: true

class Organisation
  class IsUserOrganisationOwnerService
    def initialize(organisation, user)
      @user = user
      @organisation = organisation
    end

    attr_reader :user, :organisation

    def call
      @user.id == @organisation.owner.id
    end
  end
end
