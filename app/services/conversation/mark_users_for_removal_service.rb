# frozen_string_literal: true

class Conversation
  class MarkUsersForRemovalService
    def initialize(conversation_params, conversation)
      @conversation_params = conversation_params
      @conversation = conversation
    end

    attr_reader :conversation_params

    def find_users_for_removal
      members_from_params = []
      conversation_params[:conversation_members_attributes].each do |conversation_member|
        members_from_params << conversation_member['user_id'].to_i
      end
      members_for_update = @conversation.conversation_members.map(&:user_id) + members_from_params
      members_for_destruction = []
      members_for_update.each do |member|
        members_for_destruction << member unless members_from_params.include?(member)
      end
      members_for_destruction
    end

    def call
      return unless conversation_params[:conversation_members_attributes] && !conversation_params[:conversation_members_attributes].empty?

      children_for_removal = find_users_for_removal
      @conversation.conversation_members.each do |conv_member|
        conv_member.mark_for_destruction if children_for_removal.include?(conv_member.user_id)
      end
    end
  end
end
