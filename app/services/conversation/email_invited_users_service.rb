# frozen_string_literal: true

class Conversation
  class EmailInvitedUsersService
    def initialize(conversation)
      @conversation = conversation
    end

    attr_reader :user, :conversation

    def call
      @conversation.users.each do |user|
        ConversationInvitationMailer.with(user: user, conversation: @conversation).invitation_email.deliver_later
      end
    end
  end
end
