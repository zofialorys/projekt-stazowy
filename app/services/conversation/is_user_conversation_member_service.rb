# frozen_string_literal: true

class Conversation
  class IsUserConversationMemberService
    def initialize(conversation, user)
      @user = user
      @conversation = conversation
    end

    attr_reader :user, :conversation

    def initiator?
      @user.id == @conversation.initiator_id
    end

    def call
      ConversationMember.where(conversation_id: @conversation, user_id: @user.id).count >= 1 || initiator?
    end
  end
end
