# frozen_string_literal: true

class User
  class ForceAdminToCreateOrganisationService
    def initialize(user)
      @user = user
    end

    attr_reader :user

    def user_has_organisation?
      user.is_admin && Organisation.where(user_id: user.id).present?
    end
  end
end
