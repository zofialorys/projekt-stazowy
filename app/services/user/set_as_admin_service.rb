# frozen_string_literal: true

class User
  class SetAsAdminService
    def initialize(resource)
      @user = resource
    end

    attr_reader :user

    def call
      user.is_admin = true
      user.save
    end
  end
end
