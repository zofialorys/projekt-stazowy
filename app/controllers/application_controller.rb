# frozen_string_literal: true

class ApplicationController < ActionController::Base
  before_action :set_locale, :admin_need_to_create_organisation
  before_action :configure_permitted_parameters, if: :devise_controller?

  def set_locale
    I18n.locale = params[:locale] || lang_from_request || I18n.default_locale
  end

  def default_url_options(options = {})
    { locale: I18n.locale }.merge options
  end

  def lang_from_request
    http_accept_language = request.env['HTTP_ACCEPT_LANGUAGE']
    http_accept_language.scan(/^[a-z]{2}/).first if http_accept_language.present?
  end

  def after_sign_in_path_for(resource)
    session[:admin_has_organisation] = User::ForceAdminToCreateOrganisationService.new(resource).user_has_organisation?
    if current_user.is_admin && !session[:admin_has_organisation]
      new_organisation_path
    else
      organisation_index_path
    end
  end

  private

  def admin_need_to_create_organisation
    return unless user_signed_in?

    return unless current_user.is_admin

    redirect_to(new_organisation_path) unless User::ForceAdminToCreateOrganisationService.new(current_user).user_has_organisation?
  end
end

def configure_permitted_parameters
  devise_parameter_sanitizer.permit(:invite, keys: [:team_id])
end
