# frozen_string_literal: true

class OrganisationController < ApplicationController
  before_action :authenticate_user!
  before_action :set_organisation, only: [:show, :update, :edit, :destroy]
  before_action :owner_organisation?, only: [:edit, :update, :destroy]
  before_action :can_user_create_oranisation?, only: [:new, :create]
  skip_before_action :admin_need_to_create_organisation, only: [:new, :create]

  def index
    @organisations = Organisation.all.page(params[:page])
  end

  def show
    @users = @organisation.users_from_organisation
    @is_owner = Organisation::IsUserOrganisationOwnerService.new(@organisation, current_user).call
    return unless @organisation.nil?

    flash[:error] = t('record_doesnt_exist')
    redirect_to(organisation_index_path)
  end

  def new
    @organisation = Organisation.new
  end

  def create
    @organisation = Organisation.new(organisation_params)

    if @organisation.save
      session[:admin_has_organisation] = true
      flash[:success] = t('success_update')
      redirect_to(organisation_index_path)
    else
      flash.now[:error] = t('unsuccessful_update')
      render 'new'
    end
  end

  def edit; end

  def update
    if @organisation.update(organisation_params)
      flash[:success] = t('success_update')
      redirect_to(organisation_index_path)
    else
      flash.now[:error] = t('unsuccessful_update')
      render 'edit'
    end
  end

  private

  def organisation_params
    params[:organisation].permit(:name, :description).merge(user_id: current_user.id)
  end

  def set_organisation
    @organisation = Organisation.find(params[:id])
  rescue ActiveRecord::RecordNotFound
    flash[:error] = t('record_doesnt_exist')
    redirect_to(organisation_index_path)
  end

  def owner_organisation?
    redirect_to(organisation_index_path) && return unless Organisation::IsUserOrganisationOwnerService.new(@organisation, current_user).call
  end

  def can_user_create_oranisation?
    redirect_to(organisation_index_path) && return unless current_user.is_admin && session[:admin_has_organisation] == false
  end
end
