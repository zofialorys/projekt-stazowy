# frozen_string_literal: true

# User controller class
# frozen_string_literal: true.
module Api
  module V1
    class UserController < ApplicationController
      include Response
      include ExceptionHandler
      before_action :set_user, only: [:show, :update, :destroy]
      skip_before_action :verify_authenticity_token

      # GET /user
      def index
        @users = User.all
        render json: @users, each_serializer: UserSerializer
      end

      def show
        render json: @user
      end

      # POST /user
      def create
        @user = User.create!(user_params)
        render json: @user, status: :created
      end

      # PUT /user/:id
      def update
        @user.update(user_params)
        head :no_content
      end

      # DELETE /user/:id
      def destroy
        @user.destroy
        head :no_content
      end

      private

      def user_params
        # whitelist params
        params.permit(:first_name, :last_name, :age, :email, :password, :password_confirmation)
      end

      def set_user
        @user = User.find(params[:id])
      end

      def set_user_item
        @user = @user.items.find_by!(id: params[:id]) if @user
      end
    end
  end
end
