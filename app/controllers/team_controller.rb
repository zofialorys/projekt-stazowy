# frozen_string_literal: true

class TeamController < ApplicationController
  before_action :authenticate_user!
  before_action :set_team, only: [:show, :update, :edit, :destroy]
  before_action :owner_organisation?, only: [:edit, :update, :destroy]

  def index
    @teams = current_user.user_organisation.teams.page(params[:page])
  end

  def show
    @is_owner = Organisation::IsUserOrganisationOwnerService.new(@team.organisation, current_user).call
    @users = @team.users
    return unless @team.nil?

    flash[:error] = t('record_doesnt_exist')
    redirect_to(team_index_path)
  end

  def new
    @team = Team.new
  end

  def create
    @team = Team.new(team_params)
    if @team.save
      flash[:success] = t('success_update')
      redirect_to(team_index_path)
    else
      flash.now[:error] = t('unsuccessful_update')
      render :new
    end
  end

  def edit; end

  def update
    if @team.update(team_params)
      flash[:success] = t('success_update')
      redirect_to(team_index_path)
    else
      flash.now[:error] = t('unsuccessful_update')
      render :edit
    end
  end

  private

  def team_params
    mother_organisation = Organisation.where(user_id: current_user.id).first
    params[:team].permit(:name, :description).merge(organisation_id: mother_organisation.id)
  end

  def set_team
    @team = Team.find(params[:id])
  rescue ActiveRecord::RecordNotFound
    flash[:error] = t('record_doesnt_exist')
    redirect_to(team_index_path)
  end

  def owner_organisation?
    redirect_to(team_index_path) && return unless Organisation::IsUserOrganisationOwnerService.new(@team.organisation, current_user).call
  end
end
