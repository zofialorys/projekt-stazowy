# frozen_string_literal: true

class User
  class RegistrationsController < Devise::RegistrationsController
    def create
      super
      User::SetAsAdminService.new(resource).call
    end
  end
end
