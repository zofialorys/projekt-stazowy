# frozen_string_literal: true

class User
  class ConversationsController < ApplicationController
    before_action :authenticate_user!
    before_action :set_conversation, only: [:show, :update, :edit, :destroy]
    before_action :initiator_of_conversation?, only: [:edit, :update, :destroy]
    before_action :conversation_member?, only: [:show]
    before_action :index_for_members_or_initiator, only: [:index]

    def index; end

    def show
      @is_initiator = Conversation::IsUserConversationMemberService.new(@conversation, current_user).initiator?

      return unless @conversation.nil?

      flash[:error] = t('record_doesnt_exist')
      redirect_to(user_conversations_path)
    end

    def new
      @conversation = Conversation.new
    end

    def create
      @conversation = Conversation.new(conversation_params)

      if @conversation.save
        Conversation::EmailInvitedUsersService.new(@conversation).call
        flash[:success] = t('success_update')
        redirect_to(user_conversations_path)
      else
        flash.now[:error] = t('unsuccessful_update')
        render 'new'
      end
    end

    def edit; end

    def update
      Conversation::MarkUsersForRemovalService.new(conversation_params, @conversation).call

      if @conversation.update(conversation_params)
        flash[:success] = t('success_update')
        redirect_to(user_conversations_path)
      else
        flash.now[:error] = t('unsuccessful_update')
        render 'edit'
      end
    end

    private

    def conversation_params
      params.require(:conversation)
            .permit(:subject, :note, :date, :status, :tags_as_string, :image,
                    conversation_members_attributes: [:id, :present_in_conversation, :user_id, '_destroy'])
            .merge(initiator_id: current_user.id)
    end

    def set_conversation
      @conversation = Conversation.find(params[:id])
    rescue ActiveRecord::RecordNotFound
      flash[:error] = t('record_doesnt_exist')
      redirect_to(user_conversations_path)
    end

    def initiator_of_conversation?
      redirect_to(user_conversations_path) && return unless Conversation::IsUserConversationMemberService
                                                            .new(@conversation, current_user).initiator?
    end

    def conversation_member?
      redirect_to(user_conversations_path) && return unless Conversation::IsUserConversationMemberService
                                                            .new(@conversation, current_user).call || current_user.is_admin
    end

    def index_for_members_or_initiator
      return redirect_to(user_conversations_path(initiator: 'false')) unless params[:initiator].present?

      if params[:initiator] == 'true'
        index_for_members
      else
        index_for_initiators
      end
    end

    def index_for_members
      @conversations = Conversation.conversation_initiator(current_user.id).search(params[:term]).page(params[:page])
    end

    def index_for_initiators
      @conversations = Conversation.by_conversation_members(current_user.id).search(params[:term]).page(params[:page])
    end
  end
end
