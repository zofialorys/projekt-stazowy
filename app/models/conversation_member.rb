# frozen_string_literal: true

class ConversationMember < ApplicationRecord
  belongs_to :user
  belongs_to :conversation, optional: true
end
