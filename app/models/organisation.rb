# frozen_string_literal: true

class Organisation < ApplicationRecord
  belongs_to :owner, class_name: 'User', foreign_key: :user_id
  validates_uniqueness_of :user_id
  has_many :users # , dependent: :destroy
  has_many :teams, dependent: :destroy
  validates :name, presence: true

  def users_from_organisation
    users = []
    teams.each do |team|
      users << team.users_teams.map(&:user)
    end
    users.flatten
  end
end
