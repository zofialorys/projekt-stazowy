# frozen_string_literal: true

class Team < ApplicationRecord
  has_many :users_teams, dependent: :destroy
  has_many :users, through: :users_teams, dependent: :destroy
  has_many :conversations, dependent: :destroy
  belongs_to :organisation
  validates :name, presence: true
end
