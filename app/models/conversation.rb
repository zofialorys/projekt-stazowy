# frozen_string_literal: true

class Conversation < ApplicationRecord
  scope :by_conversation_members, ->(user_id) { joins(:conversation_members).where(conversation_members: { user_id: user_id }) }
  scope :conversation_initiator, ->(user_id) { where(initiator_id: user_id) }

  belongs_to :initiator, class_name: 'User', foreign_key: :initiator_id
  belongs_to :team, optional: true

  has_many :conversation_members, autosave: true
  accepts_nested_attributes_for :conversation_members, reject_if: :member_exists?, allow_destroy: true

  has_many :users, through: :conversation_members
  has_one_attached :image

  validates :subject, presence: true
  validates :note, presence: true
  validates :date, presence: true
  validates :initiator, presence: true
  # nested attr for

  enum status: [:active, :pending, :archived]
  Gutentag::ActiveRecord.call self

  # Return the tag names separated by a comma and space
  def tags_as_string
    tag_names.join(', ')
  end

  # Split up the provided value by commas and (optional) spaces.
  def tags_as_string=(string)
    self.tag_names = string.split(/,\s*/)
  end

  def self.search(term)
    if term
      tagged_with(names: term.split(/,\s*/), match: :any)
    else
      all
    end
  end

  def self.users_conversations(user)
    by_conversation_members(user) + conversation_initiator(user)
  end

  def member_exists?(attributes)
    !conversation_members.where(user_id: attributes['user_id']).empty?
  end
end
