# frozen_string_literal: true

# User model
class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :invitable, :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
  validates :email, uniqueness: true, presence: true

  has_many :users_teams, dependent: :destroy
  has_many :teams, through: :users_teams, dependent: :destroy
  has_one :organisation

  attr_accessor :team_id

  after_invitation_created :assign_user_to_team

  def full_name
    first_name + ' ' + last_name
  end

  def assign_user_to_team
    UsersTeam.create(user_id: id, team_id: team_id)
  end

  def user_organisation
    if is_admin
      Organisation.where(user_id: id).first
    else
      teams.first.organisation
    end
  end
end
