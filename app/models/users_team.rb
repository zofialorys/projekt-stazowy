# frozen_string_literal: true

class UsersTeam < ApplicationRecord
  belongs_to :team
  belongs_to :user
end
