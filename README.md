# Project app

This is an app created for learning purposes during my internship

### Build with

* Rails 5.2.3
* Ruby 2.6.3

### Installation

This app requires Docker to run, keep in mind running scripts to create database and seed it

```sh
$ cd project
$ docker-compose up --build
```

By default, the Docker will expose port 3000, so change this within the Dockerfile if necessary. 

Verify the deployment by navigating to your server address in your preferred browser.

```sh
127.0.0.1:3000
```
Check config/env.example to find ENV variables needed to set SMTP connection with Mailgun

### Rspec tests

To run rspec test after installing your app, inside app docker container run "rspec"
```sh
$ docker ps
$ docker exec -it <docker-container-id-made-from-project_rails-app> sh
$ rspec
```

### Pipeline

Pipeline with 2 stages runs when pushing to gitlab
* stage tests:
    * rspec - job runs rspec tests
    * rubocop - job runs rubocop, free code analyzer and formatter
* stage deployment:
    * after first stage runs successfuly, app is deployed to heroku
