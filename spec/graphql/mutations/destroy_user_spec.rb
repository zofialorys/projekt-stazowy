# frozen_string_literal: true

require 'rails_helper'

module Mutations
  RSpec.describe DestroyUser, type: :request do
    describe 'resolve' do
      let!(:user) { FactoryBot.create(:user) }
      subject { post '/graphql', params: { query: query(id: user.id) } }

      it 'destroys an user' do
        expect { subject }.to change { User.count }.by(-1)
      end

      it 'returns an user' do
        subject
        json = JSON.parse(response.body)
        data = json['data']['destroyUser']
        expect(data['user']['id']).to eq(user.id.to_s)
      end
    end

    def query(id:)
      <<~GQL
        mutation {
          destroyUser(input: {
            id: #{id}
          }) {
            user {
              id
            }
          }
        }
      GQL
    end
  end
end
