# frozen_string_literal: true

require 'rails_helper'

module Mutations
  RSpec.describe UpdateUser, type: :request do
    describe 'resolve' do
      let(:user) { FactoryBot.create(:user) }
      it 'updates a user' do
        post '/graphql', params: { query: query(id: user.id) }
        expect(user.reload).to have_attributes(
          'email' => 'new_email@gmail.com'
        )
      end

      it 'returns a user' do
        post '/graphql', params: { query: query(id: user.id) }

        json = JSON.parse(response.body)
        data = json['data']['updateUser']

        expect(data['user']['email']).to eq('new_email@gmail.com')
        expect(data['user']['id']).to eq(user.id.to_s)
      end
    end

    def query(id:)
      <<~GQL
        mutation {
          updateUser(input: {
            id: #{id}
            email: "new_email@gmail.com"
          }) {
            user {
              id
              email
            }
          }
        }
      GQL
    end
  end
end
