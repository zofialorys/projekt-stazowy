# frozen_string_literal: true

require 'rails_helper'

module Mutations
  RSpec.describe CreateUser, type: :request do
    describe '.resolve' do
      let(:email) { 'zzz@fff.com' }
      let(:password) { 'password' }

      it 'creates a user' do
        expect do
          post '/graphql', params: { query: query }
        end.to change { User.count }.by(1)
      end

      it 'returns a user' do
        post '/graphql', params: { query: query }
        json = JSON.parse(response.body)
        data = json['data']['createUser']

        expect(data['user']['email']).to eq(email)
        expect(data['user']['id']).to be_present
      end
    end

    def query
      <<~GQL
        mutation {
          createUser(input: {
            email: "zzz@fff.com"
            password: "password"
          }) {
            user {
              id
              email
            }
            errors
          }
        }
      GQL
    end
  end
end
