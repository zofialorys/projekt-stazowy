# frozen_string_literal: true

require 'rails_helper'

Rspec.describe Types::UserType do
  subject { described_class }

  it { is_expected.to have_field(:id).of_type('ID!') }
  it { is_expected.to have_field(:email).of_type(String) }
end
