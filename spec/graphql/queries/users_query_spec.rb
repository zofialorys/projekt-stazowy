# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Query Users', type: :request do
  describe '.resolve' do
    let!(:user) { FactoryBot.create(:user) }
    let!(:user2) { FactoryBot.create(:user) }

    it 'returns all users' do
      post '/graphql', params: { query: query }

      json = JSON.parse(response.body)
      data = json['data']['users']
      expect(data).to eq([
                           {
                             'id' => user.id.to_s,
                             'email' => user.email
                           },
                           {
                             'id' => user2.id.to_s,
                             'email' => user2.email
                           }
                         ])
    end
  end
  def query
    <<~GQL
      query{
        users {
          id
          email
        }
      }

    GQL
  end
end
