# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Query Users', type: :request do
  describe '.resolve' do
    let!(:user) { FactoryBot.create(:user) }

    it 'return user with given ID' do
      post '/graphql', params: { query: query(id: user.id) }

      json = JSON.parse(response.body)
      data = json['data']['user']
      expect(data).to eq(
        'id' => user.id.to_s,
        'email' => user.email
      )
    end
  end
  def query(id:)
    <<~GQL
      query{
        user(id: #{id}) {
          id
          email
        }
      }

    GQL
  end
end
