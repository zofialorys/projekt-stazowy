# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'main/index.html.erb', type: :view do
  it 'displays the template correctly' do
    render

    expect(rendered).to include I18n.t('greetings')
  end
end
