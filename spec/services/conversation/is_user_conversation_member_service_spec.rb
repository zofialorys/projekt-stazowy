# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Conversation::IsUserConversationMemberService, type: :service do
  let!(:admin) { FactoryBot.create(:user, :admin) }
  let(:organisation) { FactoryBot.create(:organisation, owner: admin) }
  let!(:team) { FactoryBot.create(:team, organisation: organisation) }
  let!(:member) { FactoryBot.create(:user) }
  let!(:conversation) { FactoryBot.create(:conversation, initiator: admin, team: team) }
  let!(:conversation_member) { ConversationMember.create!(user_id: member.id, conversation_id: conversation.id) }
  let(:random_user) { FactoryBot.create(:user) }
  context '.is_initiator' do
    it 'returns true if user is initiator' do
      expect(Conversation::IsUserConversationMemberService.new(conversation, admin).initiator?).to eq(true)
    end
    it 'returns false if user is not initiator but is member' do
      expect(Conversation::IsUserConversationMemberService.new(conversation, member).initiator?).to eq(false)
    end
  end

  context '.call' do
    it 'returns true if user is member' do
      expect(Conversation::IsUserConversationMemberService.new(conversation, member).call).to eq(true)
    end
    it 'returns true if user is initiator' do
      expect(Conversation::IsUserConversationMemberService.new(conversation, admin).call).to eq(true)
    end
    it 'returns false if user is not member' do
      expect(Conversation::IsUserConversationMemberService.new(conversation, random_user).call).to eq(false)
    end
  end
end
