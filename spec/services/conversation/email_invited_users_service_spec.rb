# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Conversation::EmailInvitedUsersService, type: :service do
  include ActiveJob::TestHelper
  let!(:admin) { FactoryBot.create(:user, :admin) }
  let(:organisation) { FactoryBot.create(:organisation, owner: admin) }
  let!(:team) { FactoryBot.create(:team, organisation: organisation) }
  let!(:member) { FactoryBot.create(:user) }
  let!(:conversation) { FactoryBot.create(:conversation, initiator: admin, team: team) }
  let!(:conversation_member) { ConversationMember.create!(user_id: member.id, conversation_id: conversation.id) }
  let(:random_user) { FactoryBot.create(:user) }
  context 'Mailing users after they are added to conversation' do
    it 'matches with enqueued job' do
      expect { Conversation::EmailInvitedUsersService.new(conversation).call }
        .to(have_enqueued_job(ActionMailer::Parameterized::DeliveryJob).with { |arg| expect(arg).to eq('ConversationInvitationMailer') })
    end
  end
end
