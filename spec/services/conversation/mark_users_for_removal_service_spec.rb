# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Conversation::MarkUsersForRemovalService, type: :service do
  include ActiveJob::TestHelper
  let!(:admin) { FactoryBot.create(:user, :admin) }
  let(:organisation) { FactoryBot.create(:organisation, owner: admin) }
  let!(:team) { FactoryBot.create(:team, organisation: organisation) }
  let!(:member) { FactoryBot.create(:user) }
  let!(:member2) { FactoryBot.create(:user) }
  let!(:conversation) { FactoryBot.create(:conversation, initiator: admin, team: team) }
  let!(:conversation_member) { ConversationMember.create!(user_id: member.id, conversation_id: conversation.id) }

  context 'Marking unchecked users for removal from conversation' do
    it 'marks user for destruction' do
      params = {
        subject: 'subject',
        note: 'new conv',
        conversation_members_attributes: [{ user_id: member2.id }]
      }

      Conversation::MarkUsersForRemovalService.new(params, conversation).call
      conversation.save
      expect(conversation.users.count).to eq(0)
    end
  end
end
