# frozen_string_literal: true

RSpec.describe User::ForceAdminToCreateOrganisationService, type: :service do
  describe '.user_has_organisation?' do
    before { User.destroy_all }

    let(:user) { FactoryBot.create(:user) }
    it 'returns false if user has organisation' do
      expect(User::ForceAdminToCreateOrganisationService.new(user).user_has_organisation?).to eq(false)
    end

    it 'returns true user has organisation' do
      user.is_admin = true
      FactoryBot.create(:organisation, owner: user)

      expect(User::ForceAdminToCreateOrganisationService.new(user).user_has_organisation?).to eq(true)
    end
  end
end
