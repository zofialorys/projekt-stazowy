# frozen_string_literal: true

require 'rails_helper'

RSpec.describe User::SetAsAdminService, type: :service do
  describe '.call' do
    let(:user) { FactoryBot.create(:user) }

    it 'sets is admin to true' do
      User::SetAsAdminService.new(user).call
      expect(user.is_admin).to eq(true)
    end
  end
end
