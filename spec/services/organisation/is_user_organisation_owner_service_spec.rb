# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Organisation::IsUserOrganisationOwnerService, type: :service do
  describe '.call' do
    let(:user) { FactoryBot.create(:user) }
    let(:random_user) { FactoryBot.create(:user) }
    let(:organisation) { FactoryBot.create(:organisation, owner: user) }

    it 'returns true if user is owner' do
      expect(Organisation::IsUserOrganisationOwnerService.new(organisation, user).call).to eq(true)
    end
    it 'returns false if user is not owner' do
      expect(Organisation::IsUserOrganisationOwnerService.new(organisation, random_user).call).to eq(false)
    end
  end
end
