# frozen_string_literal: true

require 'rails_helper'

RSpec.feature 'Main page', type: :feature do
  scenario 'User visits new page' do
    visit '/'

    expect(page).to have_text(I18n.t('greetings'))
  end
end
