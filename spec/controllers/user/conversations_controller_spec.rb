# frozen_string_literal: true

require 'rails_helper'

RSpec.describe User::ConversationsController, type: :controller do
  let!(:admin) { FactoryBot.create(:user, :admin) }
  let(:organisation) { FactoryBot.create(:organisation, owner: admin) }
  let!(:team) { FactoryBot.create(:team, organisation: organisation) }
  let!(:member) { FactoryBot.create(:user) }
  let!(:member2) { FactoryBot.create(:user) }
  let!(:conversation) { FactoryBot.create(:conversation, initiator: admin, team: team) }
  let!(:conversation_member) { ConversationMember.create!(user_id: member.id, conversation_id: conversation.id) }

  before do
    sign_in(admin)
  end

  context 'GET #index do' do
    it 'returns a success response' do
      expect(response).to be_successful
    end
  end

  context 'GET #show' do
    it 'assigns the requested conversation to @Conversation' do
      get :show, params: { id: conversation }
      expect(assigns(:conversation)).to eq(conversation)
    end
  end

  context 'POST #create' do
    it 'creates conversation' do
      conversation_params = FactoryBot.attributes_for(:conversation).merge(initiator: admin)
      expect { post :create, params: { conversation: conversation_params } }
        .to change { Conversation.count }.by(1)
    end

    it 'creates conversation' do
      conversation_params = FactoryBot.attributes_for(:conversation)
                                      .merge(initiator: admin, conversation_members_attributes: [{ user_id: member2.id }])

      expect { post :create, params: { conversation: conversation_params } }
        .to change { ConversationMember.count }.by(1)
    end
  end

  context 'PUT #update/' do
    let(:subject) { 'Awesome conv' }
    let(:note) { 'New conv' }
    let(:conversation_attributes) do
      { subject: subject,
        note: note }
    end

    it 'should update conversation' do
      put :update, params: { id: conversation.id, conversation: conversation_attributes }
      conversation.reload
      expect(conversation.subject).to eql subject
      expect(conversation.note).to eql note
    end

    it 'should not update organisation because user is not the initiator' do
      sign_in(member)
      put :update, params: { id: conversation.id, conversation: conversation_attributes }
      conversation.reload
      expect(conversation.subject).not_to eq(conversation_attributes[:name])
    end

    context 'when subject is nil' do
      let(:subject) { nil }
      it 'should throw errors with valid params' do
        put :update, params: { id: conversation.id, conversation: conversation_attributes }
        expect(controller).to set_flash.now[:error]
      end
    end
    it 'should not add two identical users to conversation' do
      put :update, params: { id: conversation.id,
                             conversation: conversation_attributes.merge(conversation_members_attributes: [{ user_id: member.id }]) }
      conversation.reload
      expect(conversation.users.count).to eq(1)
      expect(conversation.users.first.id).to eq(member.id)
    end

    it 'should delete existing user from conversation' do
      put :update, params: { id: conversation.id,
                             conversation: conversation_attributes.merge(conversation_members_attributes: [{ user_id: member2.id }]) }
      conversation.reload
      expect(conversation.users.count).to eq(1)
      expect(conversation.users.first.id).to eq(member2.id)
    end
  end
end
