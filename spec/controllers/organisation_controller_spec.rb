# frozen_string_literal: true

require 'rails_helper'

RSpec.describe OrganisationController, type: :controller do
  let!(:user) { FactoryBot.create(:user) }
  let!(:admin) { FactoryBot.create(:user, :admin) }
  context 'GET #index do' do
    it 'returns a success response' do
      expect(response).to be_successful
    end
  end

  context 'GET #new do' do
    it 'should be success' do
      sign_in(admin)
      session[:admin_has_organisation] = false
      get :new
      expect(assigns(:organisation)).to_not eq nil
      expect(assigns(:organisation)).to be_a_new(Organisation)
    end
  end

  describe 'GET #show' do
    let(:organisation) { FactoryBot.create(:organisation, owner: user) }
    it 'assigns the requested organisation to @organisation' do
      sign_in(user)
      get :show, params: { id: organisation }
      expect(assigns(:organisation)).to eq(organisation)
    end
  end

  context 'GET #edit with invalid id' do
    it 'should redirect and show error flash message' do
      sign_in(user)
      get :edit, params: { id: 9999 }
      expect(controller).to set_flash[:error]
      expect(response).to redirect_to '/pl/organisation'
    end
  end

  context 'POST #create do' do
    before do
      sign_in(admin)
      session[:admin_has_organisation] = false
    end

    it 'creates organisation' do
      organisation_params = FactoryBot.attributes_for(:organisation).merge(user_id: admin.id)
      expect { post :create, params: { organisation: organisation_params } }.to change { Organisation.count }.by(1)
    end

    it 'created organisation owner is user' do
      organisation_params = FactoryBot.attributes_for(:organisation).merge(user_id: admin.id)
      post :create, params: { organisation: organisation_params }

      expect(Organisation.last.owner.id).to eq(admin.id)
    end

    it 'changes session variable' do
      organisation_params = FactoryBot.attributes_for(:organisation).merge(user_id: admin.id)
      post :create, params: { organisation: organisation_params }
      expect(session[:admin_has_organisation]).to equal(true)
    end
  end

  context 'PUT #update/' do
    let(:user2) { FactoryBot.create(:user) }
    let!(:organisation) { FactoryBot.create(:organisation, owner: user) }

    it 'should update organisation' do
      params = {
        name: 'Awesome organisation',
        description: 'new description'
      }
      sign_in(user)
      put :update, params: { id: organisation.id, organisation: params }
      organisation.reload
      expect(organisation.name).to eql params[:name]
      expect(organisation.description).to eql params[:description]
    end

    it 'should not update organisation because user is not the owner' do
      params = {
        name: 'Awesome organisation',
        description: 'new description'
      }
      sign_in(user2)
      put :update, params: { id: organisation.id, organisation: params }
      organisation.reload
      expect(organisation.name).not_to eq(params[:name])
    end

    it 'should throw errors with valid params' do
      params = {
        name: nil,
        description: 'new description'
      }
      sign_in(user)
      put :update, params: { id: organisation.id, organisation: params }
      organisation.reload
      expect(controller).to set_flash.now[:error]
    end
  end
end
