# frozen_string_literal: true

require 'rails_helper'

RSpec.describe MainController, type: :controller do
  describe 'GET #index' do
    it 'returns http success' do
      get :index, params: { locale: :pl }
      expect(response).to have_http_status(:success)
    end
    it 'returns http success' do
      get :index, params: { locale: :en }
      expect(response).to have_http_status(:success)
    end
  end
end
