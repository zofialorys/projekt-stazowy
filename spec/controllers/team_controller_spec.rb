# frozen_string_literal: true

require 'rails_helper'

RSpec.describe TeamController, type: :controller do
  let(:user) { FactoryBot.create(:user) }
  before do
    user.is_admin = true
    sign_in(user)
  end
  let(:organisation) { FactoryBot.create(:organisation, owner: user) }
  let(:team) { FactoryBot.create(:team, organisation: organisation) }

  context 'GET #index do' do
    it 'returns a success response' do
      expect(response).to be_successful
    end
  end

  context 'GET #new do' do
    it 'should be success' do
      get :new
      expect(assigns(:team)).to_not eq nil
      expect(assigns(:team)).to be_a_new(Team)
    end
  end

  describe 'GET #show' do
    it 'assigns the requested team to @tea' do
      get :show, params: { id: team }
      expect(assigns(:team)).to eq(team)
    end
  end

  context 'GET #edit with invalid id' do
    it 'should redirect and show error flash message' do
      get :edit, params: { id: 99_999 }
      expect(controller).to set_flash[:error]
      expect(response).to redirect_to '/pl/team'
    end
  end

  context 'POST #create do' do
    let(:team_params) { FactoryBot.attributes_for(:team).merge(organisation: organisation) }
    it 'creates team' do
      expect { post :create, params: { team: team_params } }.to change { Team.count }.by(1)
    end

    it 'created team belongs to organisation of user' do
      post :create, params: { team: team_params }
      expect(Team.last.organisation.owner.id).to eq(user.id)
    end
  end

  context 'PUT #update/' do
    let(:user2) { FactoryBot.create(:user) }

    it 'should update team' do
      params = {
        name: 'Awesome team',
        description: 'new description'
      }
      put :update, params: { id: team.id, team: params }
      team.reload
      expect(team.name).to eql params[:name]
      expect(team.description).to eql params[:description]
    end

    it 'should not update team because user is not the owner of team organisation' do
      params = {
        name: 'Awesome organisation',
        description: 'new description'
      }
      sign_in(user2)
      put :update, params: { id: team.id, team: params }
      team.reload
      expect(organisation.name).not_to eq(params[:name])
    end

    it 'should throw errors with invalid params' do
      params = {
        name: nil,
        description: 'new description'
      }
      put :update, params: { id: team.id, team: params }
      team.reload
      expect(controller).to set_flash.now[:error]
    end
  end
end
