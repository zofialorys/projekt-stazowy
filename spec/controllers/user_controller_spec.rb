# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Api::V1::UserController, type: :controller do
  context 'GET #index do' do
    it 'returns a success response' do
      expect(response).to be_successful
    end
  end
end
