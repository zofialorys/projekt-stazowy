# frozen_string_literal: true

# Preview all emails at http://localhost:3000/rails/mailers/conversation_invitation
class ConversationInvitationPreview < ActionMailer::Preview
end
