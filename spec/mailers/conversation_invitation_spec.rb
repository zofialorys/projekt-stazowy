# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ConversationInvitationMailer, type: :mailer do
  describe 'Conversation-invitation emails' do
    let!(:admin) { FactoryBot.create(:user, :admin) }
    let(:organisation) { FactoryBot.create(:organisation, owner: admin) }
    let!(:team) { FactoryBot.create(:team, organisation: organisation) }
    let!(:member) { FactoryBot.create(:user) }
    let!(:conversation) { FactoryBot.create(:conversation, initiator: admin, team: team) }

    subject { ConversationInvitationMailer.with(user: member, conversation: conversation).invitation_email }

    it 'renders the subject sender and recipient' do
      expect(subject.subject).to eq(I18n.t('you_are_invited_to_new_conversation'))
      expect(subject.from).to eq(['notifications@example.com'])
      expect(subject.to).to eq([member.email])
    end

    it 'renders the body' do
      expect(subject.body.encoded).to match(I18n.t('you_have_been_invited_to_attend_conversation'))
    end
  end
end
