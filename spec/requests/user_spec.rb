# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Users API', type: :request do
  # initialize test data
  before { User.destroy_all }
  let!(:users) do
    FactoryBot.create(:user)
    FactoryBot.create(:user)
  end
  let(:user_id) { User.first.id }

  # Test suite for GET /api/v1/user
  describe 'GET /user' do
    # make HTTP get request before each example
    before { get '/api/v1/user' }

    it 'returns users' do
      # Note `json` is a custom helper to parse JSON responses
      expect(json).not_to be_empty
      expect(json.size).to eq(2)
    end

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end
  end

  # Test suite for GET /api/v1/user/:id
  describe 'GET /api/v1/user/:id' do
    before { get "/api/v1/user/#{user_id}" }

    context 'when the record exists' do
      it 'returns the user' do
        expect(json).not_to be_empty
        expect(json['id']).to eq(user_id)
      end

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
    end

    context 'when the record does not exist' do
      let(:user_id) { User.last.id + 2000 }

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found message' do
        expect(response.body).to match(/Couldn't find User/)
      end
    end
  end

  # Test suite for POST /api/v1/user
  describe 'POST api/v1/user' do
    # valid payload
    let(:valid_attributes) { { first_name: 'Anna', last_name: 'Nowak', age: 22, password: 'password', password_confirmation: 'password', email: 'anna99@example.com' } }

    context 'when the request is valid' do
      before { post '/api/v1/user', params: valid_attributes }

      it 'creates a user' do
        expect(json['first_name']).to eq('Anna')
      end

      it 'returns status code 201' do
        expect(response).to have_http_status(201)
      end
    end

    context 'when the request is invalid' do
      before { post '/api/v1/user', params: { first_name: 'Anna', last_name: 'Nowak', age: 22 } }

      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'returns a validation failure message' do
        expect(response.body)
          .to match(I18n.t('activerecord.errors.messages.record_invalid'))
      end
    end
  end

  # Test suite for PUT /api/v1/user/:id
  describe 'PUT /api/v1/user/:id' do
    let(:valid_attributes) { { first_name: 'Anna', last_name: 'Nowak', age: 22, email: 'anna99@example.com' } }

    context 'when the record exists' do
      before { put "/api/v1/user/#{user_id}", params: valid_attributes }

      it 'updates the record' do
        expect(response.body).to be_empty
      end

      it 'returns status code 204' do
        expect(response).to have_http_status(204)
      end
    end

    context 'when the request is invalid' do
      before { post '/api/v1/user', params: { first_name: 'Anna', last_name: 'Nowak', age: 22 } }

      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'returns a validation failure message' do
        expect(response.body)
          .to match(I18n.t('activerecord.errors.messages.record_invalid'))
      end
    end
  end

  # Test suite for DELETE /api/v1/user/:id
  describe 'DELETE /api/v1/user/:id' do
    before { delete "/api/v1/user/#{user_id}" }

    it 'returns status code 204' do
      expect(response).to have_http_status(204)
    end
  end
end
