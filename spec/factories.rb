# frozen_string_literal: true

FactoryBot.define do
  factory :conversation_member do
    user { nil }
    conversation { nil }
  end

  factory :conversation do
    subject { 'subject' }
    note { 'MyText' }
    date { '2019-07-31' }
  end

  factory :organisation do
    name { Faker::Name.unique.first_name }
    description { 'MyText' }
    owner_id { 92 }
  end

  factory :users_team do
    team { nil }
    user { nil }
  end

  factory :team do
    name { 'test-team' }
    description { 'MyText, this is an amazing, organized, creative and reliable team' }
    organisation
  end

  factory :user do
    first_name { 'John' }
    last_name  { 'Doe' }
    email { Faker::Internet.unique.email }
    password { 'password' }
    password_confirmation { 'password' }
    age { 22 }
    is_admin { false }
    trait :admin do
      is_admin { true }
    end
  end
end
