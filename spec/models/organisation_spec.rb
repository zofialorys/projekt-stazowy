# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Organisation, type: :model do
  context 'validations' do
    it 'validates presence of name' do
      should validate_presence_of(:name)
    end
    it do
      should belong_to(:owner)
    end
  end

  describe 'users_from_organisation method' do
    let(:user) { FactoryBot.create(:user) }
    let(:organisation) { FactoryBot.create(:organisation, owner: user) }
    let!(:team) { FactoryBot.create(:team, organisation: organisation) }
    let!(:user2) { FactoryBot.create(:user) }
    let!(:users_teams) { UsersTeam.create!(user: user2, team: team) }

    it 'returns only one user' do
      expect(organisation.users_from_organisation.size).to eq(1)
    end

    it 'returns the user of the organisation' do
      expect(organisation.users_from_organisation.first.id).to eq(user2.id)
    end
  end
end
