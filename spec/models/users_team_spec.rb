# frozen_string_literal: true

require 'rails_helper'

RSpec.describe UsersTeam, type: :model do
  context 'validations' do
    it { should belong_to(:user) }
    it { should belong_to(:team) }
  end
end
