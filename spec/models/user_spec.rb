# frozen_string_literal: true

require 'rails_helper'

RSpec.describe User, type: :model do
  let(:first_name) { 'John' }
  let(:last_name) { 'Red' }
  let!(:organisation) { FactoryBot.create(:organisation, owner: user) }
  let!(:team) { FactoryBot.create(:team, organisation: organisation) }
  let!(:email) { "example@example.com" }
  let(:user) { FactoryBot.create(:user) }


  context 'with single User' do
    it '#full_name' do
      user = User.create!(first_name: first_name, last_name: last_name, email: 'emaill@example.com', password: 'password', age: 24)
      expect(user.full_name).to eq("#{first_name} #{last_name}")
    end
  end

  context 'validations' do
    before { FactoryBot.create(:user) }
    it do
      should validate_uniqueness_of(:email).ignoring_case_sensitivity
    end
  end

  context 'after invitation created'
    it "triggers #assign_user_to_team on invite" do
      expect(user).to receive(:assign_user_to_team)
      user.invite!
    end

    it "#assign_user_to_team should create users_team object" do
      expect {
        user2 = User.invite!(email: email, team_id: team.id)
        }.to change { User.count }.by(1)
    end

    it "#assign_user_to_team changes UsersTeam" do
      user2 = User.invite!(email: email, team_id: team.id)
      expect(UsersTeam.last.user_id).to eq(user2.id)
    end

    describe 'user_organisation' do
      let!(:admin) { FactoryBot.create(:user, :admin) }
      let!(:organisation) { FactoryBot.create(:organisation, owner: admin) }
      let!(:team) { FactoryBot.create(:team, organisation: organisation) }
      let!(:user2) { FactoryBot.create(:user) }
      let!(:users_teams) { UsersTeam.create!(user: user2, team: team) }
  
      it 'returns the organisation admin created' do
        expect(admin.user_organisation).to eq(organisation)
      end
  
      it 'returns the organisation that user belongs to' do
        expect(user2.user_organisation).to eq(organisation)
      end
    end
end
