# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Conversation, type: :model do
  context 'validations' do
    it { should have_many(:users).through(:conversation_members) }
    it do
      should belong_to(:initiator)
    end
    it 'validates presence of subject' do
      should validate_presence_of(:subject)
    end
    it 'validates presence of note' do
      should validate_presence_of(:note)
    end
    it 'validates presence of date' do
      should validate_presence_of(:date)
    end
    it 'validates presence of initiator' do
      should validate_presence_of(:initiator)
    end
  end

  describe 'conversation functions' do
    let!(:admin) { FactoryBot.create(:user, :admin) }
    let(:organisation) { FactoryBot.create(:organisation, owner: admin) }
    let!(:team) { FactoryBot.create(:team, organisation: organisation) }
    let!(:member) { FactoryBot.create(:user) }
    let!(:random_user) { FactoryBot.create(:user) }
    let!(:conversation) { FactoryBot.create(:conversation, initiator: admin, team: team) }
    let!(:conversation_member) { ConversationMember.create!(user_id: member.id, conversation_id: conversation.id) }

    context 'users_conversations' do
      it 'returns only one user' do
        expect(Conversation.users_conversations(admin).last).to eq(conversation)
      end

      it 'returns only one user' do
        expect(Conversation.users_conversations(member).last).to eq(conversation)
      end

      it 'returns the user of the organisation' do
        expect(Conversation.users_conversations(random_user)).to be_empty
      end
    end

    context 'tags_as_strings' do
      it '#tags_as_string returns the tag names separated by a comma and space' do
        conversation.tag_names << 'portland'
        conversation.tag_names << 'xyz'
        expect(conversation.tags_as_string).to eq('portland, xyz')
      end

      it '#tags_as_string setter splits up value by commas and spaces.' do
        expect(conversation.tags_as_string = 'business').to eq('business')
      end
    end

    context 'search conversation by tag' do
      it '#searches and returns only conversations with given tag' do
        conversation.tag_names << 'xyz'
        conversation.save
        expect(Conversation.search('xyz')).to match_array(conversation)
      end
      it '#searches and returns only conversations with given tag' do
        conversation.tag_names << 'portland'
        conversation.save
        expect(Conversation.search('no-portland')).not_to match_array(conversation)
      end
      it '#returns all conversation when the given parametr is empty' do
        terms = nil
        expect(Conversation.search(terms).all).to match_array(Conversation.all)
      end
    end
  end
end
