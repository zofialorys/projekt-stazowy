# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Team, type: :model do
  context 'validations' do
    it { should belong_to(:organisation) }
  end
end
