# frozen_string_literal: true

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
 ConversationMember.destroy_all
 Conversation.destroy_all
 Organisation.destroy_all
 User.destroy_all
 Team.destroy_all
 UsersTeam.destroy_all

password = 'user1234'

admin = User.create(first_name: Faker::Name.first_name,
                    last_name: Faker::Name.last_name,
                    email: 'admin1@example.com',
                    age: Faker::Number.number(2),
                    password: password,
                    password_confirmation: password,
                    is_admin: true)

organisation = Organisation.create!(
  name: 'Seed company',
  description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.',
  owner: admin
)

2.times do
  team = Team.create!(
    name: Faker::Name.first_name,
    organisation: organisation
  )

  8.times do
    user = User.create!(first_name: Faker::Name.first_name,
                        last_name: Faker::Name.last_name,
                        email: Faker::Internet.unique.email,
                        age: Faker::Number.number(2),
                        password: password,
                        password_confirmation: password)

    UsersTeam.create!(
      team: team,
      user: user
    )
  end
end

puts " - There are #{User.count} users." # lepiej size'a
puts " - There are #{Organisation.count} organisations." # lepiej size'a
puts " - There are #{Team.count} teams." # lepiej size'a

puts organisation.id
