class AddStatusToConversations < ActiveRecord::Migration[5.2]
  def change
    add_column :conversations, :status, :string
  end
end
