class ChangeStatusToBeEnumInConversations < ActiveRecord::Migration[5.2]
  def change
    remove_column :conversations, :status
    add_column :conversations, :status, :integer, default: 0
  end
end
