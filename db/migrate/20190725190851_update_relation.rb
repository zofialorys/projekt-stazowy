class UpdateRelation < ActiveRecord::Migration[5.2]
  def up
    remove_column :users, :organisation_id
  end

  def down
    add_column :users, :organisation_id, :integer
  end
end
