class AddUserToOrganisation < ActiveRecord::Migration[5.2]
  def change
    add_reference :organisations, :user, foreign_key: true, on_delete: :cascade

  end
end
  