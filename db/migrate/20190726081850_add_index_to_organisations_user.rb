class AddIndexToOrganisationsUser < ActiveRecord::Migration[5.2]
  def change
    remove_index :organisations, :user_id
    add_index :organisations, :user_id, unique: true
  end
end
