class CreateConversation < ActiveRecord::Migration[5.2]
  def change
    create_table :conversations do |t|
      t.string :subject
      t.text :note
      t.datetime :date
      t.references :initiator, foreign_key: {to_table: :users}
      t.references :team, foreign_key: true
      t.timestamps
    end
  end
end
