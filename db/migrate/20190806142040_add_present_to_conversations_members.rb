class AddPresentToConversationsMembers < ActiveRecord::Migration[5.2]
  def up
    add_column :conversation_members, :present_in_conversation, :boolean, default: 0
  end
  def down
    remove_column :conversation_members, :present
  end
end