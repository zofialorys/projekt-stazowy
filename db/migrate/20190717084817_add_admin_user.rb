class AddAdminUser < ActiveRecord::Migration[5.2]
  def up  
    change_table :users do |t|
      t.boolean     :is_admin, null: false, default: false
    end
  end

  def down
    change_table :users do |t|
      t.remove :is_admin
    end
  end
end
